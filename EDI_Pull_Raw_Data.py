#####################################################################
# Author: Lorenzo Ramirez, Jr.                                      #
# Date Created: Septeber 9, 2022                                    #
# Purpose: to automatically locate and save purchase orders (850)   #
#    from ECS EDI Programs using the EDI API for the finance teams  #
#####################################################################

import csv
import json
import os
import requests
import urllib.parse
from datetime import datetime, timedelta, date
from getpass import getpass
from requests.auth import HTTPBasicAuth

# FUNCTION: Saves all data from csv file into one dictionary
def create_po_dict():
    # Creates dictionary from csv file. Repeats if file cannot be found
    while True:
        try:
            # Asks user for csv file name
            csv_file = input("Please enter your CSV file ('example_name.csv') that holds your purchase order information: ")
            
            # Gathers all PO information - number, date, archived
            with open(csv_file) as csv_data:
                csv_reader = csv.reader(csv_data, delimiter=',')
                line_count = 0
                po_dict = dict()

                # Loops through each row for the PO number and date
                for row in csv_reader:
                    if line_count == 0:
                        line_count += 1
                    else:
                        csv_po = row[0]
                        csv_date = row[1]
                        po_dict["PO {}".format(line_count)] = []
                        po_dict["PO {}".format(line_count)].append([csv_po, csv_date])
                        line_count += 1
            return po_dict
        except FileNotFoundError:
            input("\nERROR: Cannot find CSV file. The file is not located in the same folder as this script or has been mistyped. Press Enter to retype it.\n")

# FUNCTION: Uses Batch API & File API to find and save purchase order content to EDI file
def locate_po(po_dict):
    # Checks if user credentials are authetic. Repeats if user is inauthenticated
    while True: 
        try: 
            # Asks for user credentials & formats credentials to http format for request usage
            username = input("\nType in your ECS username, then click Enter: ")
            password = getpass("Type in your ECS password, then click Enter: ")
            user_credentials = HTTPBasicAuth(username, password)
            
            # Checks if user is authenticated and is not used for anything else
            batch_response = requests.get("http://bfmediapp01:80/api/batch?", auth=user_credentials)
            batch_response.raise_for_status()

            # Loops through each dict key (purchase order) to locate and download PO using value (PO information)
            for key, value in po_dict.items():

                # Splits the PO number, date, and archived into separate variables
                for po_info in value:
                    po_number = po_info[0]
                    po_date = po_info[1]

                # Changes date into a range of 6 days in case users use invoice date. For active data
                try:
                    po_date_format = datetime.strptime(po_date, "%Y-%m-%d")
                except:
                    input("Purchase order {}'s PO date needs to be in the format \"YYYY-mm-DD\". Press Enter to continue to next PO...".format(po_number))
                    continue
                from_date = str(po_date_format - timedelta(3))
                to_date = str(po_date_format + timedelta(3))

                # Calls function to determines if purchase order is archived from provided date
                archived = determine_archive(po_date)

                print("\nLooking for {} from {}...".format(po_number, po_date))

                # Encodes Batch API url with filter
                batch_base = "http://bfmediapp01:80/api/batch?"
                batch_id_list = []
                if archived is False: # for active (non-archived) data
                    batch_endpoint_active = {"cmd": "query", "from-date": from_date, "to-date": to_date,
                                                "doc-type": "850", "doc-reference": po_number}
                    batch_endpoint_encoded = urllib.parse.urlencode(batch_endpoint_active)
                    batch_encoded_url = batch_base + batch_endpoint_encoded      
                elif archived is True: # for archived data
                    batch_endpoint_archived = {"cmd": "query", "from-date": "{} 00:00:00".format(po_date),
                                                "to-date": "{} 23:59:59".format(po_date), "archive": archived,
                                                "content": po_number}
                    batch_endpoint_encoded = urllib.parse.urlencode(batch_endpoint_archived)
                    batch_encoded_url = batch_base + batch_endpoint_encoded   
                 
                try:
                    # Connects to Batch API
                    batch_response = requests.get(batch_encoded_url, auth=user_credentials)
                    batch_response_dict = json.loads(batch_response.text)

                    # Saves Batch IDs from batch API get request into list
                    batch_response_length = len(batch_response_dict['batches'])
                    item_num = 0
                    while item_num is not batch_response_length:
                        batch_id_list.append(batch_response_dict['batches'][item_num]['batch']['id'])
                        item_num += 1
                except:
                    input("ERROR: Unable to find {} purchase order. Press Enter to continue to next PO...".format(po_number))
                    continue 
                
                print("Saving EDI files for {}...".format(po_number))

                # Downloads every batch ID into EDI files from batch ID list
                for batch_id in batch_id_list:
                    # Encodes File API url with batch ID and archive value
                    file_base = "http://bfmediapp01:80/api/file?"
                    file_endpoint_parameters = {"cmd": "query", "batch-id": batch_id, "archive": archived}
                    file_endpoint_encoded = urllib.parse.urlencode(file_endpoint_parameters)
                    file_encoded_url = file_base + file_endpoint_encoded

                    # Establishes connection to EDI with File API
                    po_content = requests.get(file_encoded_url, auth=user_credentials)
                    
                    # Saves content to edi file
                    edi_file = open("{}_{}_{}.edi".format(batch_id, po_number, po_date), "w")
                    edi_file.write(po_content.text)
                    edi_file.close()
            return   
        except requests.exceptions.HTTPError:
            input("ERROR: Failed Login. Your credentials be wrong. Press Enter to try again...")      

# FUNCTION: Uses the PO date to determine if it is considered archived (created more than 90 days ago)
def determine_archive(po_date):
    # Splits the PO date items for date class
    po_date_items = po_date.split("-")

    # Gets current date and splits the items for date class
    today = str(date.today())
    today_items = today.split("-")

    # Finds difference between current date and PO date
    today = date(int(today_items[0]), int(today_items[1]), int(today_items[2]))
    po_date_date = date(int(po_date_items[0]), int(po_date_items[1]), int(po_date_items[2]))
    date_diff = today - po_date_date

    # Determines if archived is turned on or off
    if date_diff.days > 90:
        archived = True
    else:
        archived = False

    return archived

# FUNCTION: calls create_po_dict() and locate_po(), and prints ending message
def main():
    # Calls function to create a dictionary of the PO information from the csv file
    po_dict = create_po_dict()

    # Calls fuctnion to connect to ECS software to find and download raw purchase orders
    locate_po(po_dict)

    # Ending message
    input("\nThe raw data of your purchase orders have been saved in \"{directory}\" as edi notepad files. "
            "\nPlease disregard any file that is not the Purchase order document. "
            "\nPress Enter to exit...".format(directory = os.getcwd()))

if __name__ == "__main__":
    main()